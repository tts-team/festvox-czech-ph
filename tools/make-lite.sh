#!/bin/sh

mkdir wav-lite

sed '1,4d' dic/phdiph.est | \
awk '
BEGIN { n=0 }
{ lines[$1]=$0 }
END {
  n=asort(lines);
  print "EST_File index"
  print "DataType ascii"
  print "NumEntries", n
  print "EST_Header_End"
  for(i=1; i<=n; i++) print lines[i]
}' > dic/phdiph.est.tmp
mv dic/phdiph.est.tmp dic/phdiph.est

awk '
/ph[0-9][0-9][0-9][0-9]/ {
  xmin=$3-0.05; xmax=$5+0.05
  if(xmin<0) xmin=0
  if(!($2 in min) || xmin<min[$2]) min[$2]=xmin
  if(!($2 in max) || xmax>max[$2]) max[$2]=xmax
}
END {
  n=asorti(min,sorted)
  for(i=1; i<=n; i++) { key=sorted[i]; print key, min[key], max[key] }
}' dic/phdiph.est | \
while true; do
  read file start end
  if [ -z "$file" ]; then break; fi
  echo $file $start $end
  file=$file.wav
  sox wav/$file -v 0 wav/tmp-suffix.wav trim ${end}s
  sox wav/$file wav/tmp.wav trim 0 ${end}s
  sox wav/tmp.wav wav/tmp-word.wav trim ${start}s
  sox wav/tmp.wav -v 0 wav/tmp-prefix.wav trim 0 ${start}s
  sox wav/tmp-prefix.wav wav/tmp-word.wav wav/tmp-suffix.wav wav-lite/$file
  rm wav/tmp-*.wav
done

mv wav wav-full
mv wav-lite wav
